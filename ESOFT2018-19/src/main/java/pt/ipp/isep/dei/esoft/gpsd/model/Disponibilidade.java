/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 *
 * @author user
 */
public class Disponibilidade {

    private Data dataInicio;
    private Tempo horaInicio;
    private Data dataFim;
    private Tempo horaFim;

    /**
     * construtor com todos os parametros
     *
     * @param dataI (data de inicio)
     * @param horaI (hora de inicio)
     * @param dataF (data de fim)
     * @param horaF (hora de fim)
     */
    public Disponibilidade(Data dataI, Tempo horaI, Data dataF, Tempo horaF) {
        if ((dataI == null) || (horaI == null) || (dataF == null)
                || (horaF == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.dataInicio = dataI;
        this.dataFim = dataF;
        this.horaInicio = horaI;
        this.horaFim = horaF;
    }

    /**
     * permite obter a data de inicio
     *
     * @return dataI (data de inicio)
     */
    public Data getDataInicio() {
        return this.dataInicio;
    }

    /**
     * permite obter a data de fim
     *
     * @return dataF (data de fim)
     */
    public Data getDataFim() {
        return this.dataFim;
    }

    /**
     * permite obter a hora de inicio
     *
     * @return horaI (hora de inicio)
     */
    public Tempo getHoraInicio() {
        return this.horaInicio;
    }

    /**
     * permite obter a hora de fim
     *
     * @return horaF (hora de fim)
     */
    public Tempo getHoraFim() {
        return this.horaFim;
    }

}
