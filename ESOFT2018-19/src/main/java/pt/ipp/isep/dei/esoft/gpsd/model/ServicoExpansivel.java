/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 * Serviço aonde duração pode variar
 *
 *
 * @author user
 */
public class ServicoExpansivel implements Servico {

    private String m_strId;
    private String m_strDescricaoBreve;
    private String m_strDescricaoCompleta;
    private double m_dCustoHora;
    private Categoria m_oCategoria;

    /**
     * Instancia o serviço expansível
     *
     * @param m_strId
     * @param m_strDescricaoBreve
     * @param m_strDescricaoCompleta
     * @param m_dCustoHora
     * @param m_oCategoria
     */
    public ServicoExpansivel(String m_strId, String m_strDescricaoBreve, String m_strDescricaoCompleta, double m_dCustoHora, Categoria m_oCategoria) {
        this.m_strId = m_strId;
        this.m_strDescricaoBreve = m_strDescricaoBreve;
        this.m_strDescricaoCompleta = m_strDescricaoCompleta;
        this.m_dCustoHora = m_dCustoHora;
        this.m_oCategoria = m_oCategoria;
    }

    /**
     * public boolean hasId(String strId) { return this.m_strId.equalsIgnoreCase(strId); }
     *
     * @Override public int hashCode() { int hash = 7; hash = 23 * hash + Objects.hashCode(this.m_strId); return hash; }
     * @Override public boolean equals(Object o) { // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
     * <p>
     * // self check if (this == o) return true; // null check if (o == null) return false; // type check and cast if (getClass() != o.getClass()) return false; // field comparison Servico obj = (Servico) o; return (Objects.equals(m_strId, obj.m_strId)); }
     *
     */
    /**
     * retorna as características do serviço
     */
    @Override
    public String toString() {
        return "Id: " + m_strId + "; Descrição Breve: " + m_strDescricaoBreve + ";Descrição Completa: " + m_strDescricaoCompleta + ";Custo Hora: " + m_dCustoHora + ";Categoria: " + m_oCategoria;
    }

    /**
     * Método que calcula o custo, consoante a duração do serviço
     *
     * @param duracao
     * @return
     */
    @Override
    public double getCustoParaDuracao(double duracao) {
        return this.m_dCustoHora * duracao;
    }

    /**
     * Procura uma categoria por id
     *
     * @param catID
     * @return true/false
     */
    @Override
    public boolean hasCatID(String catID) {
        return (this.m_oCategoria.getCodigo().equalsIgnoreCase(catID)) ? true : false;
    }

    /**
     * Procura uma serviço por identificação
     *
     * @param strID
     * @return true/false
     */
    @Override
    public boolean hasID(String strID) {
        return (this.m_strId.equalsIgnoreCase(strID)) ? true : false;
    }

    /**
     * Retorna o id de um serviço
     *
     * @return id
     */
    @Override
    public String getId() {
        return m_strId;
    }
}
