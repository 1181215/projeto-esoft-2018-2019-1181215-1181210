package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.List;
import java.util.Set;

public class CriterioDistanciaHorario implements AlogoritmoAdapter {

    public List<Atribuicao> afetaPrestadorAPedido(List<PedidoPrestacaoServicos> lpps, List<PrestadorServicos> lps) {
        double menor = 6.22 * (10 ^ 23);
        double tmp;
        for (PedidoPrestacaoServicos pedido : lpps) {
            for (PrestadorServicos prestadorServico : lps) {
                CodigoPostal codigoPedido = pedido.getEndereco().getCodigoPostal();
                Set<AreaGeografica> areas = prestadorServico.getLstAreasGeograficas().getLstAreasGeograficas();
                for (AreaGeografica area : areas) {
                    CodigoPostal codigoPrestador = area.getCodigoPostal();
                    tmp = codigoPrestador.distancia(codigoPrestador.getLatitude(), codigoPedido.getLongitude(), codigoPedido.getLatitude(), codigoPedido.getLongitude());
                    if (tmp < menor) {
                        menor = tmp;
                    }

                }
            }
        }
        return null;
    }
}
