/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoCategorias;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoClientes;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoPedidoPrestacaoServicos;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.*;

import java.util.List;

/**
 * @author user
 */


public class EfetuarPedidoPrestacaoServicosController {

    private Empresa m_oEmpresa;
    private RegistoCategorias m_oRegistoCategorias;
    private RegistoServicos m_oRegistoServicos;
    private RegistoClientes m_oRegistoClientes;
    private RegistoPedidoPrestacaoServicos m_oRegistoPedidoPrestacaoServicos;
    private PedidoPrestacaoServicos m_oPedidoPrestacaoServicos;
    //private Categoria m_oCategoria;
    private String m_oEmail;
    private Cliente m_oCliente;


    public EfetuarPedidoPrestacaoServicosController() {


        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_CLIENTE))
           throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oEmail = AplicacaoGPSD.getInstance().getSessaoAtual().getEmailUtilizador();
        this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
    }

    public List<EnderecoPostal> novoPedido() {
        AplicacaoGPSD app = AplicacaoGPSD.getInstance();
        SessaoUtilizador sessao = app.getSessaoAtual();
        this.m_oEmail = sessao.getEmailUtilizador();
        m_oRegistoClientes = m_oEmpresa.getRegistoClientes();
        m_oCliente = m_oRegistoClientes.getClienteByEmail(this.m_oEmail);
        List<EnderecoPostal> lep = m_oCliente.getEnderecosPostais();
        return lep;
    }

    public void setEndPostal(String end, CodigoPostal codPostal, String local) {
        EnderecoPostal endP = m_oCliente.getEnderecoPostal(end, codPostal, local);
        m_oRegistoPedidoPrestacaoServicos = m_oEmpresa.getRegistoPedidoPrestadorServicos();
        m_oPedidoPrestacaoServicos = m_oRegistoPedidoPrestacaoServicos.novoPedido(m_oCliente, endP);
    }

    public List<Categoria> getCategorias() {
        m_oRegistoCategorias = m_oEmpresa.getRegistoCategorias();
        return this.m_oRegistoCategorias.getCategorias();
    }

    public List<Servico> getServicosDeCategoria(String catID) {
        m_oRegistoServicos = m_oEmpresa.getRegistoServicos();
        return this.m_oRegistoServicos.getServicosDeCategoria(catID);
    }

    public boolean addPedidoServico(String idServ, String desc, double dur) {
        Servico s = this.m_oRegistoServicos.getServicoById(idServ);
        return this.m_oPedidoPrestacaoServicos.addPedidoServico(s, desc, dur);

    }

    public boolean addHorario(Data data, Tempo hora) {
        return this.m_oPedidoPrestacaoServicos.addHorario(data, hora);
    }

    public boolean valida() {
        return m_oRegistoPedidoPrestacaoServicos.validaPedido(this.m_oPedidoPrestacaoServicos);
    }

    public double getCustoTotal() {
        double c;
        return c = this.m_oPedidoPrestacaoServicos.getCustoTotal();
    }


    public int registaPedido() {
        int num;
        return num= this.m_oRegistoPedidoPrestacaoServicos.registaPedido(this.m_oPedidoPrestacaoServicos);
    }

    
}