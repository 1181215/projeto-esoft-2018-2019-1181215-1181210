/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.Registos;

import pt.ipp.isep.dei.esoft.gpsd.model.TipoServico;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;
import pt.ipp.isep.dei.esoft.gpsd.model.Servico;

/**
 * @author user
 */
public class RegistoServicos {

    private final Set<Servico> m_lstServicos;

    public RegistoServicos() {
        this.m_lstServicos = new HashSet<>();
    }

    public List<Servico> getServicosDeCategoria (String catID){

        List<Servico> ls = new ArrayList<Servico>();
        for (Servico serv : this.m_lstServicos) {
            if (serv.hasCatID(catID)) {
                ls.add(serv);
            }
        }
        return ls;
    }

    public Servico getServicoById(String strId) {
        for (Servico serv : this.m_lstServicos) {
            if (serv.hasID(strId)) {
                return serv;
            }
        }

        return null;
    }

    public boolean registaServico(Servico oServico) {
        if (this.validaServico(oServico)) {
            return addServico(oServico);
        }
        return false;
    }

    private boolean addServico(Servico oServico) {
        return m_lstServicos.add(oServico);
    }

    public boolean validaServico(Servico oServico) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    
}