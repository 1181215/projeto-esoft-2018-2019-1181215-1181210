/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 * Interface serviço que permite instanciar diferentes tipos de serviços
 *
 *
 * @author paulomaio
 */
public interface Servico {

    /**
     * Método equals para comparação
     *
     * @param o objeto;
     * @return retorna true/false
     */
    @Override
    public boolean equals(Object o);

    /**
     * método que retorna as características do serviço
     *
     * @return características do serviço
     */
    @Override
    public String toString();

    /**
     * Método que calcula o custo, consoante a duração do serviço
     *
     * @param duracao
     * @return
     */
    public double getCustoParaDuracao(double duracao);

    /**
     * Procura uma categoria por id
     *
     * @param catID
     * @return true/false
     */
    public boolean hasCatID(String catID);

    /**
     * Procura uma serviço por categoria
     *
     * @param strID
     * @return true/false
     */
    public boolean hasID(String strID);
    
    /**
     * Retorna o id de um serviço
     * 
     * @return id
     */
    public String getId();
}
