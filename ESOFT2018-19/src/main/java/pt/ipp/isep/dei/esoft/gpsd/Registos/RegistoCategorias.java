/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.Registos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.gpsd.model.Categoria;

/**
 *
 * @author user
 */
public class RegistoCategorias {

    private final Set<Categoria> m_lstCategorias = new HashSet<>();

    public Categoria getCategoriaById(String strId) {
       try {
           for (Categoria cat : this.m_lstCategorias) {
               if (cat.hasId(strId)) {
                   return cat;
               }
           }
       }catch (Exception e){
           System.out.println("A categoria não existe");
           return null;
       }
       return null;
    }

    public Categoria novaCategoria(String strCodigo, String strDescricao) {
        {
            try {
                return new Categoria(strCodigo, strDescricao);
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void registaCategoria(Categoria oCategoria) throws Exception {
        if (this.validaCategoria(oCategoria)) {
            addCategoria(oCategoria);
        }else{
        throw new Exception ("Erro ao registar a categoria");
    }
    }
    private boolean addCategoria(Categoria oCategoria) {
        return m_lstCategorias.add(oCategoria);
    }

    public boolean validaCategoria(Categoria oCategoria) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    public List<Categoria> getCategorias() {
        List<Categoria> lc = new ArrayList<>();
        lc.addAll(this.m_lstCategorias);
        return lc;
}

    // </editor-fold>
}
