package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.List;

public interface AlogoritmoAdapter {

    public List<Atribuicao> afetaPrestadorAPedido(List<PedidoPrestacaoServicos> lpps, List<PrestadorServicos> lps);
}
