package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.Set;

public class ListaAreaGeograficaPS {

    private Set<AreaGeografica> m_lstAreasGeograficas;

    /**
     * Adiciona uma areaGeografica a lista existente
     * 
     * @param ag
     * @return 
     */
    public boolean addAreaGeografica(AreaGeografica ag) {
        if (this.validaAreaGeografica(ag)) {

            return add(ag);
        }
        return false;
    }
    
    /**
     * valida uma area geografica
     * 
     * @param ag
     * @return 
     */
    private boolean validaAreaGeografica(AreaGeografica ag) {
        boolean bRet = true;
        if (ag.getRaioAtuacao() <= 0 || ag.getCustoDeslocacao() <= 0) {
            bRet = false;
        }
        if (ag.getDesignacao().isEmpty() || ag.getDesignacao() == null){
            bRet=false;
        }
        return bRet;
    }
    
    /**
     * retorna a lista de areas geograficas
     * 
     * @return 
     */
    public Set<AreaGeografica> getLstAreasGeograficas() {
        return m_lstAreasGeograficas;
    }
    
    /**
     * adiciona uma area geografica à lista
     * 
     * @param ag
     * @return 
     */
    private boolean add(AreaGeografica ag) {
        return this.m_lstAreasGeograficas.add(ag);
    }

}
