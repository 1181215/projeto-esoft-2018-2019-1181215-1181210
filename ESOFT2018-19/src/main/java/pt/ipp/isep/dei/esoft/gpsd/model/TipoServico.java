/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author user
 */
public class TipoServico {

    
    
    private String designacao; 
    private String classeServico;

    /**
     * Construtor da classe TipoServico com todos os parametros
     * @param designacao (servico limitado, fixo ou expansivel)
     * @param classeServico 
     */
    
    public TipoServico(String designacao, String classeServico) {
        {
            if ((designacao == null) || (classeServico == null) || (designacao.isEmpty()) || (classeServico.isEmpty())) {
                throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
            }

            this.designacao = designacao;
            this.classeServico = classeServico;
        }
    }

    /**
     * Verifica a existência de um tipo de serviço
     * @param idTipo designaçao do tipo
     * @return 
     */
    public boolean hasId(String idTipo) {
        return idTipo.equalsIgnoreCase(designacao);
    }

//    @Override
//    public int hashCode()
//    {
//        int hash = 7;
//        hash = 23 * hash + Objects.hashCode(this.m_strId);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
//
//        // self check
//        if (this == o)
//            return true;
//        // null check
//        if (o == null)
//            return false;
//        // type check and cast
//        if (getClass() != o.getClass())
//            return false;
//        // field comparison
//        TipoServico obj = (TipoServico) o;
//        return (Objects.equals(m_strId, obj.m_strId));
//    }

    @Override
    public String toString() {
        return "TipoServico{" + "designacao=" + designacao + ", classeServico=" + classeServico + '}';
    }

   
//
//
//    public boolean hasCatID(String catID)
//    {
//        return this.m_oCategoria.getCodigo().equalsIgnoreCase(catID);
//    }

   /**
    * cria um novo serviço
    * @param id
    * @param descB
    * @param descC
    * @param custo
    * @param cat
    * @return
    * @throws IllegalAccessException
    * @throws InvocationTargetException
    * @throws InstantiationException
    * @throws ClassNotFoundException
    * @throws NoSuchMethodException 
    */
    
    public Servico novoServico(String id, String descB, String descC, Double custo,
                               Categoria cat) throws IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException, NoSuchMethodException {
        Class<?> oClass = Class.forName(this.classeServico);
        Class[] argsClasses = new Class[]{String.class, String.class, String.class,
                Double.class, Categoria.class};
        Constructor constructor = oClass.getConstructor();
        Object[] argsValues = new Object[]{id, descB, descC, custo, cat};
        Servico serv = (Servico) constructor.newInstance(argsValues);
        return serv;
    }

    public List<TipoServico> criaTiposServicosSuportados(Properties props) {
        List<TipoServico> listTipos = new ArrayList<TipoServico>();
// Conhecer quantos TipoServico são suportados
        String qtdTipos = props.getProperty("Empresa.QuantidadeTiposServicoSuportados");
        int qtd = Integer.parseInt(qtdTipos);
// Por cada tipo suportado criar a instância respetiva
        for (int i = 1; i <= qtd; i++) {
// Conhecer informação (descrição e classe) da instância a criar
            String desc = props.getProperty("Empresa.TipoServico.”+ i +”.Designacao");
            String classe = props.getProperty("Empresa.TipoServico." + i + ".Classe");
// Criar a instância
            TipoServico tipoServ = new TipoServico(desc, classe);
// Adicionar à lista a devolver
            listTipos.add(tipoServ);
        }
// Retornar Tipos Suportados
        return listTipos;
    }
}
