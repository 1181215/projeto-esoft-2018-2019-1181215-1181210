package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.model.Horario;

public class PedidoPrestacaoServicosController {

    public void getCategorias(){}

    public void getServicos(){}

    public void novoPedidoPrestacaoServicos(int idServ, String descricao, double custosAdicionais, Horario horario){}

    public void addQuantidadeTempo(int tempo){}

    public void addHorario(Horario horario){}

    public void getCustoEstimado(){}

    public void registaPedidoPrestacaoServicos(){}

    public void searchCategoriaID(int categoriaID){}
}
