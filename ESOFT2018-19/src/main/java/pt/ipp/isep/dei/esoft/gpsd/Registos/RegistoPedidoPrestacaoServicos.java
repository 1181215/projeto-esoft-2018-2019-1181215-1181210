/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.Registos;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.model.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.gpsd.model.PedidoPrestacaoServicos;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.SerializeMain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author user
 */
public class RegistoPedidoPrestacaoServicos {

    private final Set<PedidoPrestacaoServicos> m_lstPedidoPrestacaoServicos;
    private final AutorizacaoFacade m_oAutorizacao;
    private int numeroSequencial = 0;

    public RegistoPedidoPrestacaoServicos() {
        this.m_lstPedidoPrestacaoServicos = new HashSet<>();
        this.m_oAutorizacao = new AutorizacaoFacade();
    }


    public PedidoPrestacaoServicos novoPedido(Cliente oCliente, EnderecoPostal oEndereco) {

        return new PedidoPrestacaoServicos(oCliente, oEndereco);
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }


    public boolean validaPedido(PedidoPrestacaoServicos pedido) {
        //Double c = pedido.calculaCusto();

        boolean bRet = true;

        // Escrever aqui o código de validação

        if (pedido == null)
            bRet = false;

        //

        return bRet;
    }

    public int registaPedido(PedidoPrestacaoServicos ped) {
        this.validaPedido(ped);
        int num = this.geraNumeroPedido();
        ped.setM_oNumero(num);
        this.addPedido(ped);
        this.notificaCliente(ped);
        return num;
    }

    private int geraNumeroPedido() {
        return this.numeroSequencial++;
    }

    private void addPedido(PedidoPrestacaoServicos ped) {

        SerializeMain.guardarPedidos(this.m_lstPedidoPrestacaoServicos);
        this.m_lstPedidoPrestacaoServicos.add(ped);
    }

    private void notificaCliente(PedidoPrestacaoServicos ped) {
        System.out.println("O pedido foi registado com sucesso. Os dados foram enviados por email.");
    }

    public List<PedidoPrestacaoServicos> getPedidosSubmetidos() {
        List<PedidoPrestacaoServicos> lpps = new ArrayList<>();
        lpps.addAll(this.m_lstPedidoPrestacaoServicos);
        return lpps;
    }
}
