/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.io.Serializable;

/**
 *Preferencia do horario do pedido de prestacao de serviços
 * 
 * @author user
 */
public class PreferenciaHorario implements Serializable {
    
    private int int_ordem;
    private Data str_data;
    private Tempo str_hora;
    
    /**
     * instancia a preferencia do horario
     * 
     * @param ordem
     * @param data
     * @param hora 
     */
    public PreferenciaHorario(int ordem, Data data, Tempo hora){
        this.int_ordem=ordem;
        this.str_data=data;
        this.str_hora=hora;
           
    }

    /** 
     * retorna a ordem da preferencia
     * 
     * @return 
     */
    public int getInt_ordem() {
        return int_ordem;
    }
    /**
     * retorna uma data da preferencia
     * 
     * @return 
     */
    public Data getStr_data() {
        return str_data;
    }
    /**
     * retorna uma hora da preferencia
     * 
     * @return 
     */
    public Tempo getStr_hora() {
        return str_hora;
    }
}
