 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Classe que representa clientes
 * @author paulomaio
 */
public class Cliente implements Serializable
{
    private String m_strNome;
    private String m_strNIF;
    private String m_strTelefone;
    private String m_strEmail;
    private List<EnderecoPostal> m_lstMoradas = new ArrayList<EnderecoPostal>();
            
    
    /**
     * construtor de um cliente com todos os parâmetros
     * @param strNome (o nome do cliente)
     * @param strNIF (nif do cliente)
     * @param strTelefone (telefone do cliente)
     * @param strEmail (email do cliente)
     * @param oMorada (morada do cliente)
     */
    
    
    public Cliente(String strNome, String strNIF, String strTelefone, String strEmail, EnderecoPostal oMorada)
    {
        if ( (strNome == null) || (strNIF == null) || (strTelefone == null) ||
                (strEmail == null) || (oMorada == null) ||
                (strNome.isEmpty())|| (strNIF.isEmpty()) || (strTelefone.isEmpty()) || 
                (strEmail.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strNome = strNome;
        this.m_strEmail = strEmail;
        this.m_strNIF = strNIF;
        this.m_strTelefone = strTelefone;
        m_lstMoradas.add(oMorada);
    }
    
   /**
    * permite obter o nome do cliente
    * @return nome (nome do cliente)
    */
    
    public String getNome()
    {
        return this.m_strNome;
    }
    
   /**
    * permite obter o email de um cliente
    * @return email (email do cliente)
    */
    
    public String getEmail()
    {
        return this.m_strEmail;
    }
    
   
    /**
     * verifica se existe um cliente através de um email enviado como parâmetro
     * @param strEmail (email do cliente a procurar)
     * @return 
     */
    
    public boolean hasEmail(String strEmail)
    {
        return this.m_strEmail.equalsIgnoreCase(strEmail);
    }
   
    
    /**
     * adiciona um endereco postal, ou seja, uma morada a um cliente
     * @param oMorada (morada a adicionar)
     * @return true se adicionou ou false se nao adicionou
     */
    public boolean addEnderecoPostal(EnderecoPostal oMorada)
    {
        return this.m_lstMoradas.add(oMorada);
    }
    
    
    /**
     * remove um endereco postal, ou seja, uma morada a um cliente
     * @param oMorada (morada a remover)
     * @return true se removeu ou false se nao removeu
     */
    
    public boolean removeEnderecoPostal(EnderecoPostal oMorada)
    {
        return this.m_lstMoradas.remove(oMorada);
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.m_strEmail);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Cliente obj = (Cliente) o;
        return (Objects.equals(m_strEmail, obj.m_strEmail) || Objects.equals(m_strNIF, obj.m_strNIF));
    }
    
    
    /**
     * imprime a informacao detalhada de um cliente
     * @return informacao
     */
    
    @Override
    public String toString()
    {
        String str = String.format("%s - %s - %s - %s", this.m_strNome, this.m_strNIF, this.m_strTelefone, this.m_strEmail);
        for(EnderecoPostal morada:this.m_lstMoradas)
            str += "\nMorada:\n" + morada.toString();
        return str;
    }
    
    
    /**
     * cria um endereco postal
     * @param strLocal (representa um local)
     * @param strCodPostal (representa um codigo postal)
     * @param localidade (representa uma localidade)
     * @return um novo endereco postal
     */
    
    public static EnderecoPostal novoEnderecoPostal(String strLocal, CodigoPostal strCodPostal, String localidade)
    {
        return new EnderecoPostal(strLocal,strCodPostal, localidade);
    }

    /**
     * permite obter uma lista dos vários endereços postais que um cliente possui
     * @return lista com as moradas
     */
    
    public List<EnderecoPostal> getEnderecosPostais(){
        return this.m_lstMoradas;
    }
    
    /**
     * permite obter um endereco postal de um cliente, através de parametros
     * @param end (endereço)
     * @param codPostal (codigo postal)
     * @param local (localidade)
     * @return endereço 
     */
    
    public EnderecoPostal getEnderecoPostal (String end, CodigoPostal codPostal, String local ){
        for (int i=0; i<this.m_lstMoradas.size();i++){
            if (this.m_lstMoradas.get(i).getCodigoPostal().equals(codPostal) && this.m_lstMoradas.get(i).getLocal().equalsIgnoreCase(local) && this.m_lstMoradas.get(i).getLocalidade().equalsIgnoreCase(end)){
                return this.m_lstMoradas.get(i);
            }
                       
        }
        return null;
    }
}
