/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoAreasGeograficas;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoCategorias;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoClientes;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

/**
 * @author paulomaio
 */
public class AplicacaoGPSD {

    // Inspirado em https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
    private static AplicacaoGPSD singleton = null;
    private final Empresa m_oEmpresa;
    private final AutorizacaoFacade m_oAutorizacao;

    private AplicacaoGPSD() {
        Properties props = getProperties();
        this.m_oEmpresa = new Empresa(props.getProperty(Constantes.PARAMS_EMPRESA_DESIGNACAO),
                props.getProperty(Constantes.PARAMS_EMPRESA_NIF));
        this.m_oAutorizacao = this.m_oEmpresa.getAutorizacaoFacade();
        bootstrap();
    }

    public static AplicacaoGPSD getInstance() {
        if (singleton == null) {
            synchronized (AplicacaoGPSD.class) {
                singleton = new AplicacaoGPSD();
            }
        }
        return singleton;
    }

    public Empresa getEmpresa() {
        return this.m_oEmpresa;
    }

    public SessaoUtilizador getSessaoAtual() {
        return this.m_oAutorizacao.getSessaoAtual();
    }

    public boolean doLogin(String strId, String strPwd) {
        return this.m_oAutorizacao.doLogin(strId, strPwd) != null;
    }

    public void doLogout() {
        this.m_oAutorizacao.doLogout();
    }

    private Properties getProperties() {
        Properties props = new Properties();

        // Adiciona propriedades e valores por omissão
        props.setProperty(Constantes.PARAMS_EMPRESA_DESIGNACAO, "Default Lda.");
        props.setProperty(Constantes.PARAMS_EMPRESA_NIF, "Default NIF");

        // Lê as propriedades e valores definidas
        try {
            InputStream in = new FileInputStream(Constantes.PARAMS_FICHEIRO);
            props.load(in);
            in.close();
        } catch (Exception ex) {

        }
        return props;
    }

    private void bootstrap() {
        CodigoPostal codPostalRic = new CodigoPostal("3880", 12414.4, 4124.5);
        EnderecoPostal moradaRic = new EnderecoPostal("casa", codPostalRic, "casa");

        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_ADMINISTRATIVO);
        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_CLIENTE);
        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_FRH);
        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_PRESTADOR_SERVICO);

        carregarCategorias();
        carregarCodigoPostal();
        carregarServicos();
        lerFicheiroClientes();

        this.m_oAutorizacao.registaUtilizadorComPapel("Administrativo 1", "adm1@esoft.pt", "123456", Constantes.PAPEL_ADMINISTRATIVO);
        this.m_oAutorizacao.registaUtilizadorComPapel("Administrativo 2", "adm2@esoft.pt", "123456", Constantes.PAPEL_ADMINISTRATIVO);

        this.m_oAutorizacao.registaUtilizadorComPapel("FRH 1", "frh1@esoft.pt", "123456", Constantes.PAPEL_FRH);
        this.m_oAutorizacao.registaUtilizadorComPapel("FRH 2", "frh2@esoft.pt", "123456", Constantes.PAPEL_FRH);

        this.m_oAutorizacao.registaUtilizadorComPapeis("Martim", "martim@esoft.pt", "123456", new String[]{Constantes.PAPEL_FRH, Constantes.PAPEL_ADMINISTRATIVO});
        this.m_oAutorizacao.registaUtilizadorComPapel("Ricardo", "Ricardo@gmail.com", "1234", Constantes.PAPEL_CLIENTE);
        this.m_oAutorizacao.registaUtilizadorComPapel("Xavier", "xavier@gmail.com", "1234", Constantes.PAPEL_PRESTADOR_SERVICO);
    }

    public void carregarCategorias() {
        RegistoCategorias rc = m_oEmpresa.getRegistoCategorias();
        try {
            Scanner carregarCat = new Scanner(new File(Constantes.FICH_CATEGORIAS));
            while (carregarCat.hasNextLine()) {
                String linha = carregarCat.nextLine();
                if ((linha.trim()).length() > 0) {
                    String[] tmp = linha.split(";");
                    String codigo = tmp[0].trim();
                    String designacao = tmp[1].trim();
                    Categoria cat = rc.novaCategoria(codigo, designacao);
                    rc.registaCategoria(cat);
                }
            }
            carregarCat.close();
        } catch (FileNotFoundException e) {
            System.out.println("Sem ficheiro Categorias.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void carregarAreasGeograficas(CodigoPostal cod) {
        RegistoAreasGeograficas ra = this.m_oEmpresa.getRegistoAreasGeograficas();
        try {
            Scanner carregarArea = new Scanner(new File(Constantes.FICH_AREAS_GEOGRAFICAS));
            String linha = carregarArea.nextLine();
            if ((linha.trim()).length() > 0) {
                String[] tmp = linha.split(";");
                String designacao = tmp[0].trim();
                double custo = Double.parseDouble(tmp[1].trim());
                double raio = Double.parseDouble(tmp[2].trim());
                AreaGeografica ag = ra.novaAreaGeografica(designacao, custo, cod, raio);
                ra.registaAreaGeografica(ag);

            }
            carregarArea.close();
        } catch (FileNotFoundException e) {
            System.out.println("Sem ficheiro AreasGeograficas.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void carregarServicos() {
        RegistoCategorias rc = this.m_oEmpresa.getRegistoCategorias();
        RegistoServicos rs = this.m_oEmpresa.getRegistoServicos();
        try {
            Scanner carregarServico = new Scanner((new File(Constantes.FICH_SERVICOS)));
            while (carregarServico.hasNextLine()) {
                String linha = carregarServico.nextLine();
                if ((linha.trim()).length() > 0) {
                    String[] tmp = linha.split(";");
                    String servId = tmp[0].trim();
                    String descB = tmp[1].trim();
                    String descC = tmp[2].trim();
                    double custoHora = Double.parseDouble(tmp[3].trim());
                    String catId = tmp[4].trim();
                    Categoria cat = rc.getCategoriaById(catId);
                    String tipoId = tmp[5].trim();
                    if (tipoId.equalsIgnoreCase("Fixo")) {
                        int tempo = Integer.parseInt(tmp[6].trim());
                        ServicoFixo servF = new ServicoFixo(servId, descB, descC, custoHora, cat, tempo);
                        rs.registaServico(servF);
                    } else if (tipoId.equalsIgnoreCase("Limitado")) {
                        ServicoLimitado servL = new ServicoLimitado(servId, descB, descB, custoHora, cat);
                        rs.registaServico(servL);
                    } else {
                        ServicoExpansivel servE = new ServicoExpansivel(servId, descB, descB, custoHora, cat);
                        rs.registaServico(servE);
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Sem Ficheiro Servicos.txt");
        }
    }

    public void carregarCodigoPostal() {
        RegistoAreasGeograficas registoAreasGeograficas = this.m_oEmpresa.getRegistoAreasGeograficas();
        try {
            Scanner carregarCodigo = new Scanner(new File(Constantes.FICH_CODIGOS_POSTAIS));
            while (carregarCodigo.hasNextLine()) {
                String linha = carregarCodigo.nextLine();
                if ((linha.trim()).length() > 0) {
                    String[] tmp = linha.split(";");
                    String codigo = tmp[0].trim();
                    Double latitude = Double.parseDouble(tmp[1].trim());
                    Double longitude = Double.parseDouble(tmp[2].trim());
                    CodigoPostal codigoPostal = new CodigoPostal(codigo, latitude, longitude);
                    carregarAreasGeograficas(codigoPostal);
                }
            }
            carregarCodigo.close();
        } catch (FileNotFoundException e) {
            System.out.println("Sem ficheiro Categorias.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AutorizacaoFacade getAutorizacao() {
        return m_oAutorizacao;
    }

   /** public void carregarClientes() {
        RegistoClientes registoClientes = this.m_oEmpresa.getRegistoClientes();
        try {
            Scanner carregarCliente = new Scanner(new File(Constantes.FICH_CLIENTES));
            while (carregarCliente.hasNextLine()) {
                String linha = carregarCliente.nextLine();
                if ((linha.trim()).length() > 0) {
                    String[] tmp = linha.split(";");
                    String nome=tmp[0].trim();
                    String nif=tmp[1].trim();
                    String telemovel=tmp[2].trim();
                    String email=tmp[3].trim();
                    String local=tmp[4].trim();
                    String codigoPostal=tmp[5].trim();
                    double latitude=Double.parseDouble(tmp[6].trim());
                    double longitude=Double.parseDouble(tmp[7].trim());
                    CodigoPostal codigoPostal1=new CodigoPostal(codigoPostal,432523,134234);
                    String localidade=tmp[8].trim();
                    EnderecoPostal enderecoPostal=new EnderecoPostal(local,codigoPostal1,localidade);
                    String pass=tmp[9].trim();
                    Cliente cliente=registoClientes.novoCliente(nome,nif,telemovel,email,enderecoPostal);
                    registoClientes.registaCliente(cliente,pass);
                    this.m_oAutorizacao.registaUtilizadorComPapel(nome, email, pass, Constantes.PAPEL_CLIENTE);
                }
            }carregarCliente.close();
        }catch (FileNotFoundException e){
            System.out.println("Ficheiro clientes não encontrado");
        }catch (Exception e) {
            e.printStackTrace();
        }
    }**/
    
    private void lerFicheiroClientes() {

        this.m_oAutorizacao.registaPapelUtilizador(Constantes.PAPEL_CLIENTE);

        try {
            Scanner s = new Scanner(new File("ficheiros/clientes.txt"));
            s.nextLine();
            while (s.hasNextLine()) {
                String linha = s.nextLine();
                if (!linha.equalsIgnoreCase("")) {
                    String[] vecLinha = linha.split(";");
                    String strNome = vecLinha[0].trim();
                    String strEmail = vecLinha[4].trim();
                    String strPass = vecLinha[5].trim();
                    this.m_oAutorizacao.registaUtilizadorComPapel(strNome, strEmail, strPass, Constantes.PAPEL_CLIENTE);
                }
            }
            s.close();
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro dos clientes não existe!");
        }

    }
}

