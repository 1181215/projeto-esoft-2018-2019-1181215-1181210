package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import java.util.Date;
import pt.ipp.isep.dei.esoft.gpsd.controller.IndicarDisponibilidadeDiariaController;
import pt.ipp.isep.dei.esoft.gpsd.model.Data;
import pt.ipp.isep.dei.esoft.gpsd.model.Tempo;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

public class IndicarDisponibilidadeDiariaUI {
    
    private IndicarDisponibilidadeDiariaController m_oController;
    
     public IndicarDisponibilidadeDiariaUI() {
        m_oController = new IndicarDisponibilidadeDiariaController();
    }
     
     public void run() {
        System.out.println("\nIndicar Disponibilidade Diária:");

        if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_oController.registaPeriodoDisponibilidade()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
       private boolean introduzDados() {
            System.out.println("Introduza o periodo de disponibilidade: ");
            System.out.println("Data Inicio: ");
            int anoI = Utils.readIntegerFromConsole("Ano: ");
            int mesI=Utils.readIntegerFromConsole("Mês: ");
            int diaI=Utils.readIntegerFromConsole("Dia: ");
            Data dataI=new Data(anoI,mesI,diaI);
            System.out.println("HoraInicio: ");
            int horaI=Utils.readIntegerFromConsole("Hora: ");
            int minutosI=Utils.readIntegerFromConsole("Minutos: ");
            int segundosI=Utils.readIntegerFromConsole("Segundos: ");
            Tempo horarioInicio=new Tempo(horaI,minutosI,segundosI);
            System.out.println("Data Fim: ");
            int anoF = Utils.readIntegerFromConsole("Ano: ");
            int mesF=Utils.readIntegerFromConsole("Mês: ");
            int diaF=Utils.readIntegerFromConsole("Dia: ");
            Data dataF=new Data(anoF,mesF,diaF);
            System.out.println("HoraInicio: ");
            int horaF=Utils.readIntegerFromConsole("Hora: ");
            int minutosF=Utils.readIntegerFromConsole("Minutos: ");
            int segundosF=Utils.readIntegerFromConsole("Segundos: ");
            Tempo horarioFim=new Tempo(horaF,minutosF,segundosF);
            
            
       
            return m_oController.novoPeriodoDisponibilidade(dataI, horarioInicio, dataF, horarioFim);
   }
       
       private void apresentaDados() {
        System.out.println("\nIndicar Disponibilidade Diária:\n" + m_oController.toString());
    }
}