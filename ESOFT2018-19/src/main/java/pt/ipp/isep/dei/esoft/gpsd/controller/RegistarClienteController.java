/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoClientes;
import pt.ipp.isep.dei.esoft.gpsd.model.*;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author paulomaio
 */
public class RegistarClienteController
{
    private AplicacaoGPSD m_oApp;
    private Empresa m_oEmpresa;

    private Cliente m_oCliente;
    private String m_strPwd;
    private RegistoClientes m_oRegistoClientes;
    public RegistarClienteController()
    {
//        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_CLIENTE))
//            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
    }
    
    
    public boolean novoCliente(String strNome, String strNIF, String strTelefone, String strEmail, String strPwd, String strLocal, CodigoPostal strCodPostal, String strLocalidade)
    {
        try
        {
            this.m_oRegistoClientes=this.m_oEmpresa.getRegistoClientes();
            this.m_strPwd = strPwd;
            EnderecoPostal morada = Cliente.novoEnderecoPostal(strLocal, strCodPostal, strLocalidade);
            this.m_oCliente =this.m_oRegistoClientes.novoCliente(strNome, strNIF, strTelefone, strEmail, morada);
            return this.m_oRegistoClientes.validaCliente(this.m_oCliente,this.m_strPwd);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCliente = null;
            return false;
        }
    }
    
    public boolean addEnderecoPostal(String strLocal, CodigoPostal strCodPostal, String strLocalidade)
    {
        if (this.m_oCliente != null)
        {
            try
            {
                EnderecoPostal morada = Cliente.novoEnderecoPostal(strLocal, strCodPostal, strLocalidade);
                return this.m_oCliente.addEnderecoPostal(morada);
            }
            catch(RuntimeException ex)
            {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } 
        return false;
    }
    
    public boolean registaCliente()
    {
        return this.m_oRegistoClientes.registaCliente(this.m_oCliente, this.m_strPwd);
    }

    public String getClienteString()
    {
        return this.m_oCliente.toString();
    }
}
