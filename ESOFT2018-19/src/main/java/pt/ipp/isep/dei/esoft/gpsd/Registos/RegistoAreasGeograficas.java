/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.Registos;

import pt.ipp.isep.dei.esoft.gpsd.model.AreaGeografica;
import pt.ipp.isep.dei.esoft.gpsd.model.CodigoPostal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


/**
 * @author user
 */
public class RegistoAreasGeograficas {

    private final Set<AreaGeografica> m_lstAreasGeograficas = new HashSet<>();

    public AreaGeografica novaAreaGeografica(String desig, double custo, CodigoPostal codPostal, double raio) {
        {
            try {
                return new AreaGeografica(desig, custo, raio, codPostal);
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void registaAreaGeografica(AreaGeografica areaGeo  ){
        this.m_lstAreasGeograficas.add(areaGeo);
    }

    public AreaGeografica getAreaGeograficaMaisPerto(CodigoPostal cp) {
        double menor =6.22*(10^23);
        AreaGeografica areaMaisPerto=new AreaGeografica();
        ArrayList<Double> distancias = new ArrayList<Double>();
        for (AreaGeografica area : m_lstAreasGeograficas) {
            CodigoPostal codigoArea = area.getCodigoPostal();
            double tmp = cp.distancia(cp.getLatitude(), cp.getLongitude(), codigoArea.getLatitude(), codigoArea.getLongitude());
            if (tmp<area.getRaioAtuacao()&&tmp < menor) {
                menor = tmp;
                areaMaisPerto = area;
            }
        }
        return areaMaisPerto;
    }
}
