package pt.ipp.isep.dei.esoft.gpsd.model;

public class Candidatura {

    private String nome;
    private int nif;
    private String email;
    private int telemovel;

    public Candidatura(String nome, int nif, String email, int telemovel) {
        this.nome = nome;
        this.nif = nif;
        this.email = email;
        this.telemovel = telemovel;
    }

    public void addCategoria(Categoria cat){}

    public static void novasHabilitacoes(String habilitacoes,DocumentosComprovativos documentosComprovativos ){}

    public void addHabProf(HabilitacoesAcademicas habilitacoesAcademicas) {
    }

    public void addHabAcad(HabilitacoesAcademicas habilitacoesAcademicas) {
    }

    public static void novoEndereco(String end,DocumentosComprovativos codPostal,String localidade ){}

    public void addEnderecoPostal(EnderecoPostal enderecoPostal) {
    }

}
