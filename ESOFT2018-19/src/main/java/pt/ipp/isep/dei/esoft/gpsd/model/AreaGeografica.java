/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 * Classe representativa de uma area geografica (possui designacao, custo de deslocação, um raio onde atua e um codigo postal
 *
 * @author
 */
public class AreaGeografica {

    private String designacao;
    private double custoDeslocacao;
    private double raioAtuacao;
    private CodigoPostal codigoPostal;

    /**
     * Construtor Area Geografica sem parametros
     */
    public AreaGeografica() {
    }

    /**
     * Permite obter a designação de uma area geografica
     *
     * @return designaçao (a designacao da area geografica)
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * Construtor de uma Area Geografica com todos os parametros
     *
     * @param designacao (a designacao da area geografica)
     * @param custoDeslocacao (custo de deslocação para essa area)
     * @param raioAtuacao (Raio em que a area atua)
     * @param codigoPostal (codigo postal do centro da area geografica)
     */
    public AreaGeografica(String designacao, double custoDeslocacao, double raioAtuacao, CodigoPostal codigoPostal) {
        if ((designacao == null) || (custoDeslocacao == 0) || (raioAtuacao == 0)
                || (designacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.designacao = designacao;
        this.custoDeslocacao = custoDeslocacao;
        this.raioAtuacao = raioAtuacao;
        this.codigoPostal = codigoPostal;
    }

    /**
     * permite obter o codigo postal
     *
     * @return codigoPostal
     */
    public CodigoPostal getCodigoPostal() {
        return this.codigoPostal;
    }

    /**
     * permite obter o custo de deslocação para uma area geografica
     *
     * @return custoDeslocacao (custo de deslocação para essa area)
     */
    public double getCustoDeslocacao() {
        return this.custoDeslocacao;
    }

    /**
     * permite obter o raio de atuacao de uma area geografica
     *
     * @return (Raio em que a area atua)
     */
    public double getRaioAtuacao() {
        return raioAtuacao;
    }
}
