/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoAreasGeograficas;
import pt.ipp.isep.dei.esoft.gpsd.controller.AplicacaoGPSD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Pedido de prestacao de serviços, feito pelo o cliente
 * 
 * 
 * @author user
 */
public class PedidoPrestacaoServicos implements Serializable {
    

    private int m_oNumero;
    private Data m_aData;
    private int m_oTotal;
    private Cliente m_oCliente;
    private EnderecoPostal m_oEndereco;
    private List<DescricaoServicoPedido> m_lstDescricaoServicoPedido;
    private List<OutroCusto> m_lstOutroCusto;
    private Empresa m_oEmpresa;
    private List<PreferenciaHorario> m_lstPreferenciaHorario;
    private double custo;
    private int ordem = 0;
    /**
     * intancia o pedido de prestacao de servicos 
     * 
     * @param cliente
     * @param endPostal 
     */
    public PedidoPrestacaoServicos(Cliente cliente, EnderecoPostal endPostal) {
        this.m_oCliente = cliente;
        this.m_oEndereco = endPostal;
        this.m_lstDescricaoServicoPedido = new ArrayList<>();
        this.m_lstOutroCusto = new ArrayList<>();
        this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
        this.m_lstPreferenciaHorario = new ArrayList<>();
    }
    /**
     * adiciona o pedidodo de serviço
     * 
     * @param serv
     * @param desc
     * @param dur
     * @return true/false
     */
    public boolean addPedidoServico(Servico serv, String desc, double dur) {
        DescricaoServicoPedido d = new DescricaoServicoPedido(serv, desc, dur);
        if (this.validaPedidoServico(d)) {
            this.addPedidoServico(d);
            return true;
        }
        return false;
    }
    
    /**
     * adiciona um pedido 
     * 
     * @param d 
     */
    private void addPedidoServico(DescricaoServicoPedido d) {
        this.m_lstDescricaoServicoPedido.add(d);
    }
    
    /**
     * valida uma pedido de servico
     * 
     * @param d
     * @return 
     */
    private boolean validaPedidoServico(DescricaoServicoPedido d) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        if (d.getDescricao() == null || d.getDuracao() <= 0 || d.getServico() == null) {
            bRet = false;
        }
        if (d.getDescricao().isEmpty()) {
            bRet = false;
            //

        }

        return bRet;
    }
    
    /**
     * metodos que permitem calcular o custo do pedido
     * 
     * @return 
     */
    public double calculaCusto() {
        this.custo = 0;
        for (DescricaoServicoPedido desc : this.m_lstDescricaoServicoPedido) {
            custo += desc.getCusto();
        }

        this.m_lstOutroCusto.clear();
        RegistoAreasGeograficas rag = this.m_oEmpresa.getRegistoAreasGeograficas();
        CodigoPostal cp = this.m_oEndereco.getCodigoPostal();
        AreaGeografica ag = rag.getAreaGeograficaMaisPerto(cp);
        double custoDeslocacao = ag.getCustoDeslocacao();
        OutroCusto oc = new OutroCusto("deslocacao", custoDeslocacao);
        this.m_lstOutroCusto.add(oc);
        return custo = custo + custoDeslocacao;
    }
    /**
     * verifica o pedido
     * 
     * @param pedido 
     */
    private void verificaPedido(PedidoPrestacaoServicos pedido) {

    }
    
    /**
     * retorna o custo total do pedido
     * 
     * @return 
     */
    public double getCustoTotal() {
        double c=calculaCusto();
        return c;
    }
    
    /**
     * retorna as caracteristicas do pedido
     * 
     * @return 
     */
    @Override
    public String toString() {
        return "PedidoPrestacaoServicos{" + "m_oNumero=" + m_oNumero + ", m_aData=" + m_aData + ", m_oTotal=" + m_oTotal + ", m_oCliente=" + m_oCliente + ", m_oEndereco=" + m_oEndereco + ", m_aDescricaoServicoPedido=" + m_lstDescricaoServicoPedido + ", m_lstDescricaoServicoPedido=" + m_lstDescricaoServicoPedido + ", m_lstOutroCusto=" + m_lstOutroCusto + ", m_oEmpresa=" + m_oEmpresa + '}';
    }
/**
 * adiciona um horario ao pedido
 * 
 * 
 * @param data
 * @param hora
 * @return 
 */
    public boolean addHorario(Data data, Tempo hora) {
        ordem = countHorarios();
        PreferenciaHorario horario = new PreferenciaHorario(ordem, data, hora);
        if (this.validaHorario(horario)) {
            this.addHorario(horario);
            return true;
        }
        return false;
    }
    
    /**
     * valida o horario do pedido
     * @param horario
     * @return 
     */
    private boolean validaHorario(PreferenciaHorario horario) {
        boolean bRet = true;

//        // Escrever aqui o código de validação
//        if (horario.getStr_data() == null || horario.getInt_ordem() <= 0 || horario.getStr_hora() == null) {
//            bRet = false;
//        }
//        if (horario.getStr_hora().isEmpty() || horario.getStr_data().isEmpty()) {
//            bRet = false;
//            //
//
//        }

        return bRet;
    }
    
    /**
     * adiciona um horario ao pedido
     * 
     * @param horario 
     */
    private void addHorario(PreferenciaHorario horario) {
        m_lstPreferenciaHorario.add(horario);
    }
    
    /**
     * retorna um contagem dos horarios 
     * @return 
     */
    public int countHorarios() {
        ordem++;
        return ordem;
    }
    
    /**
     * atribui o valor recebido ao número do pedido
     * 
     * @param m_oNumero 
     */
    public void setM_oNumero(int m_oNumero) {
        this.m_oNumero = m_oNumero;
    }
    
    /**
     * retorna o endereco postal do pedido
     * 
     * @return 
     */
    public EnderecoPostal getEndereco() {
        return m_oEndereco;
    }
    
    /**
     * lista com a preferencia de horarios do pedido
     * 
     * @return 
     */
    public List<PreferenciaHorario> getPreferenciaHorario() {
        return m_lstPreferenciaHorario;
    }
}
