/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoCategorias;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoServicos;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoTipoServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.*;

import java.util.List;

/**
 * @author paulomaio
 */
public class EspecificarServicoController {
    private Empresa m_oEmpresa;
    private Servico m_oServico;
    private TipoServico m_oTipoServico;
    private RegistoTipoServicos m_oRegistoTipoServicos;
    private RegistoCategorias m_oRegistoCategorias;
    private RegistoServicos m_oRegistoServicos;

    public EspecificarServicoController() {
        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
    }


    public List<TipoServico> getTiposServico() {
        this.m_oRegistoTipoServicos = this.m_oEmpresa.getRegistoTipoServicos();
        return this.m_oRegistoTipoServicos.getTipoServicos();
    }

    public void setTipoServico(String idTipo) {
        this.m_oTipoServico = this.m_oRegistoTipoServicos.getTipoServicoById(idTipo);
    }

    public List<Categoria> getCategorias() {
        this.m_oRegistoCategorias = this.m_oEmpresa.getRegistoCategorias();
        return this.m_oRegistoCategorias.getCategorias();
    }

    public boolean novoServico(String strId, String strDescricaoBreve, String strDescricaoCompleta, double dCustoHora, String categoriaId) throws Exception {
        Categoria cat = this.m_oRegistoCategorias.getCategoriaById(categoriaId);
        this.m_oServico = this.m_oTipoServico.novoServico(strId, strDescricaoBreve, strDescricaoCompleta, dCustoHora, cat);
        return true;
    }


    public boolean valida() {
        this.m_oRegistoServicos = this.m_oEmpresa.getRegistoServicos();
        return this.m_oRegistoServicos.validaServico(this.m_oServico);
    }

    public boolean registaServico() {
        return this.m_oRegistoServicos.registaServico(this.m_oServico);
    }

    public String getServicoString() {
        return this.m_oServico.toString();
    }
}
