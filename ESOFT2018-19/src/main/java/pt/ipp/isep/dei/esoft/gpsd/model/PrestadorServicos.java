/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.beans.binding.Bindings;
import pt.ipp.isep.dei.esoft.gpsd.model.Disponibilidade;

/**
 * Utilizador que vai prestar os serviços
 *
 *
 * @author user
 */
public class PrestadorServicos {

    private int nMecanografico;
    private String nomeCompleto;
    private String nomeAbreviado;
    private String emailInstitucional;
    private final Set<Disponibilidade> m_lstDisponibilidade;
    private ListaAreaGeograficaPS m_lstAreasGeograficas;

    /**
     * instancia um prestador de serviços
     *
     * @param nMec
     * @param nomeComp
     * @param nomeAbrev
     * @param emailInstitucional
     */
    public PrestadorServicos(int nMec, String nomeComp, String nomeAbrev, String emailInstitucional) {
        this.nMecanografico = nMec;
        this.nomeCompleto = nomeComp;
        this.nomeAbreviado = nomeAbrev;
        this.emailInstitucional = emailInstitucional;
        this.m_lstDisponibilidade = new HashSet<>();
    }

    /**
     * Retorna a lista de disponibilidades diarias de um dado prestador de serviços
     *
     * @return m_lstDisponibilidade (lista de disponibilidades diarias de um dado prestador de serviços)
     */
    public Set<Disponibilidade> getListaDisponibilidade() {
        return this.m_lstDisponibilidade;

    }

    /**
     * Nova disponibilidade de horario para o prestador
     *
     * @param dataI
     * @param horaI
     * @param dataF
     * @param horaF
     * @return
     */
    public Disponibilidade novoPedidoDisponibilidade(Data dataI, Tempo horaI, Data dataF, Tempo horaF) {
        Disponibilidade disponibilidade = new Disponibilidade(dataI, horaI, dataF, horaF);
        return disponibilidade;
    }

    /**
     * regista o periodo de disponibilidade
     *
     * @param disp
     * @return
     */
    public boolean registaPeriodoDisponibilidade(Disponibilidade disp) {
        if (this.validaPeriodoDisponibilidade(disp)) {

            return addPeriodoDisponibilidade(disp);
        }
        return false;
    }

    /**
     * valida o periodo Disponibilidade
     *
     * @param disp
     * @return
     */
    private boolean validaPeriodoDisponibilidade(Disponibilidade disp) {
        boolean bRet = true;

        if (disp.getHoraFim() == null || disp.getDataInicio() == null || disp.getDataFim() == null || disp.getHoraInicio() == null) {
            bRet = false;

        } 
            return bRet;
        }
    /**
     * adiciona um periodo de disponibilidade
     * 
     * @param disp
     * @return 
     */
    public boolean addPeriodoDisponibilidade(Disponibilidade disp) {
        return this.m_lstDisponibilidade.add(disp);
    }
    
    /**
     *Verifica se tem um certo email
     * 
     * @param email
     * @return 
     */
    public boolean hasEmail(String email) {
        return this.emailInstitucional.equalsIgnoreCase(email);
    }
    /**
     * retorna uma lista de areas geograficas
     * 
     * @return 
     */
    public ListaAreaGeograficaPS getLstAreasGeograficas() {
        return m_lstAreasGeograficas;
    }
}
