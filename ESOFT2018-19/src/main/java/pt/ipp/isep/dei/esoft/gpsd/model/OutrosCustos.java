package pt.ipp.isep.dei.esoft.gpsd.model;

public class OutrosCustos {

    private String designacao;
    private double valor;

    public OutrosCustos(String designacao, double valor) {
        this.designacao = designacao;
        this.valor = valor;
    }
}
