/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.gpsd.model.Data;
import pt.ipp.isep.dei.esoft.gpsd.model.Tempo;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Utils {
    static public String readLineFromConsole(String strPrompt) {
        try {
            System.out.println("\n" + strPrompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public int readIntegerFromConsole(String strPrompt) {
        do {
            try {
                String strInt = readLineFromConsole(strPrompt);

                int iValor = Integer.parseInt(strInt);

                return iValor;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    static public double readDoubleFromConsole(String strPrompt) {
        do {
            try {
                String strDouble = readLineFromConsole(strPrompt);

                double dValor = Double.parseDouble(strDouble);

                return dValor;
            } catch (NumberFormatException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    static public Date readDateFromConsole(String strPrompt) {
        do {
            try {
                String strDate = readLineFromConsole(strPrompt);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                Date date = df.parse(strDate);

                return date;
            } catch (ParseException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }
    
    static public Tempo readTempoFromConsole(String strPrompt) {
        do {
            try {
                String strHorario = readLineFromConsole(strPrompt);
                String[] horario = strPrompt.split("-");
                int hora= Integer.parseInt(horario[0]);
                int minutos = Integer.parseInt (horario[1]);
                int segundos= Integer.parseInt (horario[0]);

                Tempo horarioEmTempo = new Tempo (hora, minutos, segundos);
                

                return horarioEmTempo;
            } catch (ArrayIndexOutOfBoundsException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } while (true);
    }
     
    static public Data readDataFromConsole(String strPrompt) {
        do {
            try {
                String strHorario = readLineFromConsole(strPrompt);
                String[] data = strPrompt.split("-");
                int ano= Integer.parseInt(data[0]);
                int mes = Integer.parseInt (data[1]);
                int dia= Integer.parseInt (data[0]);

                Data dataEmClasse = new Data (ano, mes, dia);
                

                return dataEmClasse;
            } catch (ArrayIndexOutOfBoundsException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } while (true);
    }

    static public boolean confirma(String sMessage) {
        String strConfirma;
        do {
            strConfirma = Utils.readLineFromConsole("\n" + sMessage + "\n");
        } while (!strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n"));

        return strConfirma.equalsIgnoreCase("s");
    }

    static public Object apresentaESeleciona(List list, String sHeader) {
        apresentaLista(list, sHeader);
        return selecionaObject(list);
    }

    static public int apresentaESelecionaIndex(List list, String sHeader) {
        apresentaLista(list, sHeader);
        return selecionaIndex(list);
    }

    static public void apresentaLista(List list, String sHeader) {
        System.out.println(sHeader);

        int index = 0;
        for (Object o : list) {
            index++;

            System.out.println(index + ". " + o.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    static public Object selecionaObject(List list) {
        String opcao="";
        int nOpcao;
        do {
            try {
                opcao = Utils.readLineFromConsole("Introduza opção: ");
            } catch (NumberFormatException ex) {
                System.out.println("Numero de categoria errado");
            }
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > list.size());

        if (nOpcao == 0) {
            return null;
        } else {
            return list.get(nOpcao - 1);
        }
    }

    static public int selecionaIndex(List list) {
        String opcao;
        int nOpcao;
        do {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > list.size());

        return nOpcao - 1;
    }
}