/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.Registos.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * classe que define a empresa responsável pela prestação dos serviços
 * 
 * 
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Empresa implements Serializable {

    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<Cliente> m_lstClientes;
    private final Set<Categoria> m_lstCategorias;
    private final Set<Servico> m_lstServicos;
    private final RegistoTipoServicos r_oRegistoTipoServicos;
    private final RegistoCategorias r_oRegistoCategorias;
    private final RegistoClientes r_oRegistoClientes;
    private final RegistoPrestadorServicos r_oRegistoPrestadorServicos;
    private final RegistoAreasGeograficas r_oRegistoAreasGeograficas;
    private final RegistoPedidoPrestacaoServicos r_oRegistoPedidoPrestadorServicos;
    private final RegistoServicos r_oRegistoServicos;
    private final RegistoAtribuicoes r_oRegistoAtribuicoes;
    private String m_strDesignacao;
    private String m_strNIF;
    
    /**
     * instância a empresa
     * 
     * @param strDesignacao
     * @param strNIF 
     */
    public Empresa(String strDesignacao, String strNIF) {
        if ((strDesignacao == null) || (strNIF == null)
                || (strDesignacao.isEmpty()) || (strNIF.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;
        this.m_strNIF = strNIF;

        this.m_oAutorizacao = new AutorizacaoFacade();
        this.r_oRegistoTipoServicos = new RegistoTipoServicos();
        this.r_oRegistoCategorias = new RegistoCategorias();
        this.r_oRegistoClientes = new RegistoClientes();
        this.r_oRegistoPrestadorServicos = new RegistoPrestadorServicos();
        this.r_oRegistoAreasGeograficas = new RegistoAreasGeograficas();
        this.r_oRegistoPedidoPrestadorServicos = new RegistoPedidoPrestacaoServicos();
        this.r_oRegistoServicos = new RegistoServicos();
        this.r_oRegistoAtribuicoes = new RegistoAtribuicoes();
        this.m_lstClientes = new HashSet<>();
        this.m_lstCategorias = new HashSet<>();
        this.m_lstServicos = new HashSet<>();
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }

    //RegistoTipoServicos
    public RegistoTipoServicos getRegistoTipoServicos() {
        return this.r_oRegistoTipoServicos;
    }

    //RegistoClientes
    public RegistoClientes getRegistoClientes() {
        return this.r_oRegistoClientes;
    }

    //RegistoServicos
    public RegistoServicos getRegistoServicos() {
        return this.r_oRegistoServicos;
    }

    // RegistoCategorias
    public RegistoCategorias getRegistoCategorias() {
        return this.r_oRegistoCategorias;

    }

    //RegistoPedidoPrestadorServicos
    public RegistoPedidoPrestacaoServicos getRegistoPedidoPrestadorServicos() {
        return this.r_oRegistoPedidoPrestadorServicos;
    }

    // RegistoPrestadorServicos
    public RegistoPrestadorServicos getRegistoPrestadorServicos() {
        return this.r_oRegistoPrestadorServicos;
    }

    // RegistoAreasGeograficas
    public RegistoAreasGeograficas getRegistoAreasGeograficas() {
        return this.r_oRegistoAreasGeograficas;
    }

    //RegistoAtribuicoes
    public RegistoAtribuicoes getRegistoAtribuicoes() {
        return this.r_oRegistoAtribuicoes;
    }
}
