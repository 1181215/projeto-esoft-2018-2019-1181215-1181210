/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.Registos;

import pt.ipp.isep.dei.esoft.gpsd.model.TipoServico;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author user
 */
public class RegistoTipoServicos {

    private final Set<TipoServico> m_lstTipoServicos;

    public RegistoTipoServicos() {
        this.m_lstTipoServicos = new HashSet<>();
    }

    public List<TipoServico> getTipoServicos() {
        List<TipoServico> lc = new ArrayList<>();
        lc.addAll(this.m_lstTipoServicos);
        return lc;
    }

    public TipoServico getTipoServicoById(String idTipo) {
        for (TipoServico tipo : this.m_lstTipoServicos) {
            if (tipo.hasId(idTipo)) {
                return tipo;
            }
        }

        return null;
    }
}


