/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.Registos;

import pt.ipp.isep.dei.esoft.gpsd.model.PrestadorServicos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author user
 */
public class RegistoPrestadorServicos {

    private final Set<PrestadorServicos> m_lstPrestadorServicos;

    public RegistoPrestadorServicos() {
        this.m_lstPrestadorServicos = new HashSet<>();
    }

    public PrestadorServicos getPrestadorServicos(String email) {

        for (PrestadorServicos prest : this.m_lstPrestadorServicos) {
            if (prest.hasEmail(email)) {
                return prest;
            }
        }

        return null;
    }

    public List<PrestadorServicos> getListaPrestadorServicosAtivos() {
        List<PrestadorServicos> lps = new ArrayList<>();
        lps.addAll(this.m_lstPrestadorServicos);
        return lps;
    }
}
