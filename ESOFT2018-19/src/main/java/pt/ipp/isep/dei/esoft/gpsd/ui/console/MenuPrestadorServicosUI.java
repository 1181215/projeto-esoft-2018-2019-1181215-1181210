/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;

/**
 *
 * @author xavim
 */
public class MenuPrestadorServicosUI {

  

//    public MenuPrestadorServicoUI() {
//    }

    public  void run() {
        List<String> options = new ArrayList<String>();
        options.add("Ver Servicos");
        options.add("Adicionar Disponibilidade");
        // Adicionar Aqui Outras Opções

        int opcao = 0;
        do {
            opcao = Utils.apresentaESelecionaIndex(options, "\n\nMenu Prestador de Serviços");

            switch (opcao) {
                case 1:
                    IndicarDisponibilidadeDiariaUI uiD = new IndicarDisponibilidadeDiariaUI();
                    uiD.run();
                    break;
            }

        } while (opcao != -1);
    }
    
}

