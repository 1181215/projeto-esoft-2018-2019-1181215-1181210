/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *Uma classes com as características necessárias para defenir um endereço postal
 * 
 * 
 * @author paulomaio
 */
public class EnderecoPostal implements Serializable
{
    private String m_strLocal;
    private CodigoPostal m_strCodPostal;
    private String m_strLocalidade;
    
            
    /**
     * instancia um endereço postal
     * 
     * @param strLocal
     * @param strCodPostal
     * @param strLocalidade 
     */
    public EnderecoPostal(String strLocal, CodigoPostal strCodPostal, String strLocalidade)
    {
        if ( (strLocal == null) || (strCodPostal == null) || (strLocalidade == null) ||
                (strLocal.isEmpty())|| (strLocalidade.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strLocal = strLocal;
        this.m_strCodPostal = strCodPostal;
        this.m_strLocalidade = strLocalidade;
    }
    
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.m_strLocal,this.m_strCodPostal, this.m_strLocalidade);
        return hash;
    }
    
    /**
     * verifica se dois endereços sao iguais 
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        EnderecoPostal obj = (EnderecoPostal) o;
        return (Objects.equals(m_strLocal, obj.m_strLocal) && 
                Objects.equals(m_strCodPostal, obj.m_strCodPostal) &&
                Objects.equals(m_strLocalidade, obj.m_strLocalidade));
    }
    
    /**
     * retorna as características do endereço
     * 
     * @return 
     */
    @Override
    public String toString()
    {
        return String.format("%s\n%s-%s", this.m_strLocal,this.m_strCodPostal.toString(), this.m_strLocalidade);
    }
    
    /**
     * retorna o codigo postal
     * 
     * @return 
     */
    public CodigoPostal getCodigoPostal (){
        return this.m_strCodPostal;
    }
    
    /**
     * retorna a local
     * 
     * @return 
     */
    public String getLocal(){
        return this.m_strLocal;
    }
    /**
     * retorna a localidade
     * 
     * @return 
     */
    public String getLocalidade(){
        return this.m_strLocalidade;
    }
}
