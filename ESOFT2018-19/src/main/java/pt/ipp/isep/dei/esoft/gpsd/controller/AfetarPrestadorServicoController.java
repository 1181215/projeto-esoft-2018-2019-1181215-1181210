package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoAtribuicoes;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoPedidoPrestacaoServicos;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoPrestadorServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.*;

import java.util.List;

public class AfetarPrestadorServicoController {
    private Empresa m_oEmpresa;
    private RegistoPedidoPrestacaoServicos r_oRegistoPedidoPrestacaoServicos;
    private RegistoPrestadorServicos r_oRegistoPrestadorServicos;
    private RegistoAtribuicoes r_oRegistoAtribuicoes;
    private List<PedidoPrestacaoServicos> lst_PedidosPrestacaoServicos;
    private List<PrestadorServicos> lst_PrestadorServicos;
    private List<Atribuicao> lst_Atribuicoes;

    public AfetarPrestadorServicoController() {
        this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
    }

    public void run() {
        this.r_oRegistoPedidoPrestacaoServicos = this.m_oEmpresa.getRegistoPedidoPrestadorServicos();
        this.r_oRegistoPrestadorServicos = this.m_oEmpresa.getRegistoPrestadorServicos();
        this.r_oRegistoAtribuicoes=this.m_oEmpresa.getRegistoAtribuicoes();
        this.lst_PedidosPrestacaoServicos = this.r_oRegistoPedidoPrestacaoServicos.getPedidosSubmetidos();
        this.lst_PrestadorServicos = this.r_oRegistoPrestadorServicos.getListaPrestadorServicosAtivos();
    }
}
