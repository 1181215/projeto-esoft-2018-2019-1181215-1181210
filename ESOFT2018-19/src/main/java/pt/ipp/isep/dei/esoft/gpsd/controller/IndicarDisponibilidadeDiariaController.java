/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoClientes;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoPedidoPrestacaoServicos;
import pt.ipp.isep.dei.esoft.gpsd.Registos.RegistoPrestadorServicos;
import pt.ipp.isep.dei.esoft.gpsd.model.*;

import java.util.List;
import java.util.Set;


/**
 * @author user
 */
public class IndicarDisponibilidadeDiariaController {

    private Empresa m_oEmpresa;
    private RegistoClientes m_oRegistoClientes;
    private RegistoPrestadorServicos m_oRegistoPrestadorServicos;
    private PedidoPrestacaoServicos m_oPedidoPrestacaoServicos;
    private PrestadorServicos m_oPrestadorServicos;
    private Set<Disponibilidade> m_lstDisponibilidade;
    private Disponibilidade m_oDisponibilidade;
    private String emailInstitucuinal;

    public IndicarDisponibilidadeDiariaController() {
        if (!AplicacaoGPSD.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_PRESTADOR_SERVICO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oEmpresa = AplicacaoGPSD.getInstance().getEmpresa();
    }


    public void indicarNovasDisponibilidades() {
        AplicacaoGPSD app = AplicacaoGPSD.getInstance();
        SessaoUtilizador sessao = app.getSessaoAtual();
        String email = sessao.getEmailUtilizador();
        this.m_oRegistoPrestadorServicos=this.m_oEmpresa.getRegistoPrestadorServicos();
        this.m_oPrestadorServicos=this.m_oRegistoPrestadorServicos.getPrestadorServicos(email);
        
    }

    public boolean novoPeriodoDisponibilidade(Data dataI, Tempo horaI, Data dataF, Tempo horaF) {
        this.m_lstDisponibilidade = m_oPrestadorServicos.getListaDisponibilidade();
        this.m_oDisponibilidade = this.m_oPrestadorServicos.novoPedidoDisponibilidade(dataI, horaI, dataF, horaF);
        return this.m_oPrestadorServicos.registaPeriodoDisponibilidade(m_oDisponibilidade);
        
    }

    public boolean registaPeriodoDisponibilidade() {
        return this.m_oPrestadorServicos.registaPeriodoDisponibilidade(this.m_oDisponibilidade);
    }
}
