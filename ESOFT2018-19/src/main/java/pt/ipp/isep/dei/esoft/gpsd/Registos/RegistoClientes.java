/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.Registos;

import java.util.HashSet;
import java.util.Set;

import pt.ipp.isep.dei.esoft.gpsd.controller.AplicacaoGPSD;
import pt.ipp.isep.dei.esoft.gpsd.model.Cliente;
import pt.ipp.isep.dei.esoft.gpsd.model.Constantes;
import pt.ipp.isep.dei.esoft.gpsd.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.gpsd.model.Empresa;

/**
 *
 * @author user
 */
public class RegistoClientes {


    private final Set<Cliente> m_lstClientes;
    private final AutorizacaoFacade m_oAutorizacao;
    
    public RegistoClientes (){
        this.m_lstClientes=new HashSet<>();
        this.m_oAutorizacao= new AutorizacaoFacade();
    }
     
    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }
    
    public Cliente getClienteByEmail(String strEMail)
    {
        for(Cliente cliente : this.m_lstClientes)
        {
            if (cliente.hasEmail(strEMail))
            {
                return cliente;
            }
        }
        
        return null;
    }

    public Cliente novoCliente(String strNome, String strNIF, String strTelefone, String strEmail, EnderecoPostal morada)
    {
        return new Cliente(strNome, strNIF, strTelefone, strEmail, morada);
    }

    public boolean registaCliente(Cliente oCliente, String strPwd)
    {
        if (this.validaCliente(oCliente,strPwd))
        {
            if (this.getAutorizacaoFacade().registaUtilizadorComPapel(oCliente.getNome(),oCliente.getEmail(), strPwd, Constantes.PAPEL_CLIENTE))
                return addCliente(oCliente);
        }
        return false;
    }

    private boolean addCliente(Cliente oCliente)
    {
        return m_lstClientes.add(oCliente);
    }
    
    public boolean validaCliente(Cliente oCliente,String strPwd)
    {
        boolean bRet = true;

        // Escrever aqui o código de validação
        if (this.getAutorizacaoFacade().existeUtilizador(oCliente.getEmail()))
            bRet = false;
        if (strPwd == null)
            bRet = false;
        if (strPwd.isEmpty())
            bRet = false;
        //
      
        return bRet;
    }
    
}
