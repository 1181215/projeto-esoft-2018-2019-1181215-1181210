/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.io.Serializable;

/**
 *Outros custos do pedido de Prestacao de serviços
 * 
 * 
 * @author user
 */
public class OutroCusto implements Serializable {
    
    private String m_designacao;
    private double m_valor;
    
    /**
     * instancia os custos
     * 
     * @param designacao
     * @param valor 
     */
    public OutroCusto (String designacao, double valor){
        this.m_designacao=designacao;
        this.m_valor=valor;
    }
    
    /**
     * retorna a deseignacao dos outros custos
     * 
     * @return 
     */
    public String getM_designacao() {
        return m_designacao;
    }
    
    /**
     * retorna o valor dos custos
     * 
     * @return 
     */
    public double getM_valor() {
        return m_valor;
    }
}
