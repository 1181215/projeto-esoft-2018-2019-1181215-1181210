package pt.ipp.isep.dei.esoft.gpsd.model;

/**
 * Classe representativa de um Codigo Postal
 *
 * @author user
 */
public class CodigoPostal {

    private String codPostal;
    private double latitude;
    private double longitude;

//    public CodigoPostal(String codPostal) {
//        this.codPostal = codPostal;
//    }
    /**
     * Contrutor de um codigo postal com todos os parametros
     *
     * @param codPostal (codigo postal)
     * @param latitude (latitude do local)
     * @param longitude (longitude do local)
     */
    public CodigoPostal(String codPostal, double latitude, double longitude) {
        if ((codPostal == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.codPostal = codPostal;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Imprime a informaçao detalhada de um codigo postal
     *
     * @return informação
     */
    @Override
    public String toString() {
        return "CodigoPostal{" + "O código Postal: " + codPostal + ", tem uma latitude de: " + latitude + " e tem uma longitude de:" + longitude + '}';
    }

    /**
     * permite obter um codigo postal
     *
     * @return codPostal
     */
    public String getCodigoPostal() {
        return this.codPostal;
    }

    /**
     * permite obter a distancia entre codigos postais
     *
     * @param lat1 (latitude de um primeiro codigo postal)
     * @param lon1 (longitude de um primeiro codigo postal)
     * @param lat2 (latitude de um segundo codigo postal)
     * @param lon2 (longitude de um segundo codigo postal)
     * @return distancia
     */
    public double distancia(double lat1, double lon1, double lat2, double lon2) {
        // shortest distance over the earth’s surface
        // https://www.movable-type.co.uk/scripts/latlong.html
        final double R = 6371e3;
        double theta1 = Math.toRadians(lat1);
        double theta2 = Math.toRadians(lat2);
        double deltaTheta = Math.toRadians(lat2 - lat1);
        double deltaLambda = Math.toRadians(lon2 - lon1);
        double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2)
                + Math.cos(theta1) * Math.cos(theta2)
                * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c; // distância em metros
        return d;
    }

    /**
     * permite obter a latitude de um codigo postal
     *
     * @return latitude (a latitude de um codigo postal)
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * permite obter a longitude de um codigo postal
     *
     * @return longitude (a longitude de um codigo postal)
     */
    public double getLongitude() {
        return longitude;
    }
}
