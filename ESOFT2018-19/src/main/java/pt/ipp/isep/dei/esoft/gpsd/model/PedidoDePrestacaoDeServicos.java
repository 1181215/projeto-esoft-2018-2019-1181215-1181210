package pt.ipp.isep.dei.esoft.gpsd.model;

public class PedidoDePrestacaoDeServicos {

    private String descricaoDaTarefa;
    private int numeroSequencial;
    private double custoEstimado;

    public PedidoDePrestacaoDeServicos(String descricaoDaTarefa, int numeroSequencial, double custoEstimado) {
        this.descricaoDaTarefa = descricaoDaTarefa;
        this.numeroSequencial = numeroSequencial;
        this.custoEstimado = custoEstimado;
    }

    public void newHorario(Horario horario){}

    public void novaQuantidadeTempo(int tempo){}

    public void addHorario(Horario outroHorario){}

    public void getCustoEstimado(){}
}
