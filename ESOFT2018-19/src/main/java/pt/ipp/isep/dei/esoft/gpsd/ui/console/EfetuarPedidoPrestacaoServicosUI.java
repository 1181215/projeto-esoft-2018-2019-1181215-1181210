/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.ui.console;

import pt.ipp.isep.dei.esoft.gpsd.controller.EfetuarPedidoPrestacaoServicosController;
import pt.ipp.isep.dei.esoft.gpsd.model.*;
import pt.ipp.isep.dei.esoft.gpsd.ui.console.utils.Utils;
import java.util.List;

/**
 * @author user
 */
public class EfetuarPedidoPrestacaoServicosUI {

    private EfetuarPedidoPrestacaoServicosController m_controller;

    public EfetuarPedidoPrestacaoServicosUI() {
        m_controller = new EfetuarPedidoPrestacaoServicosController();
    }

    public void run() {
        m_controller.novoPedido();
        System.out.println("\nEfetuar Pedido Prestacao Serviços:");

        if (introduzDados()) {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaPedido()<0) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        } else {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        System.out.println("\nIntroduza um endereço postal:");
        String strLocal = Utils.readLineFromConsole("Rua/Av.: ");
        String strCodPostal = Utils.readLineFromConsole("Cod. Postal: ");
        double latitude=Utils.readDoubleFromConsole("Latitude: ");
        double longitude=Utils.readDoubleFromConsole("Longitude: ");
        String strLocalidade = Utils.readLineFromConsole("Localidade: ");
        CodigoPostal codigoPostal=new CodigoPostal(strCodPostal,latitude,longitude);
        m_controller.setEndPostal(strLocal,codigoPostal,strLocalidade);

        System.out.println("\nSeleciona pelo menos uma categoria:");
        int count = 0;
        do {

            List<Categoria> lc = m_controller.getCategorias();
            String catId = "";
            Categoria c = (Categoria) Utils.apresentaESeleciona(lc, "Selecione a categoria a que o serviço pertence:");
            if (c != null) {
                catId = c.getCodigo();
            }

            String servId = "";
            List<Servico> ls = m_controller.getServicosDeCategoria(catId);
            Servico s = (Servico) Utils.apresentaESeleciona(ls, "Selecione o serviço, e introduza uma descriçao completa, uma descrição breve e a respetiva duração");
           if (s != null) {
                servId = s.getId();
            }
            String desc = Utils.readLineFromConsole("Descrição do serviço: ");

            double dur = Utils.readDoubleFromConsole("Duração do Serviço: ");

            if (!m_controller.addPedidoServico(servId, desc, dur)) {
                System.out.println("Ocorreu um erro ao adicionar o serviço ao pedido.");
            } else {
                count++;
            }

        } while ((count == 0) || Utils.confirma("Pretende introduzir outro Serviço (S/N)?"));

        System.out.println("\nSeleciona pelo menos um horário:");
        int count1 = 0;
        do {
            System.out.println("Data: ");
            int ano = Utils.readIntegerFromConsole("Ano: ");
            int mes=Utils.readIntegerFromConsole("Mês: ");
            int dia=Utils.readIntegerFromConsole("Dia: ");
            Data data=new Data(ano,mes,dia);
            System.out.println("Hora: ");
            int hora=Utils.readIntegerFromConsole("Hora: ");
            int minutos=Utils.readIntegerFromConsole("Minutos: ");
            int segundos=Utils.readIntegerFromConsole("Segundos: ");
            Tempo horario=new Tempo(hora,minutos,segundos);
            if (!m_controller.addHorario(data,horario)) {
                System.out.println("Ocorreu um erro ao adicionar o horário ao pedido.");
            } else {
                count1++;
            }
        } while ((count1 == 0) || Utils.confirma("Pretende introduzir outro Horário (S/N)"));
        return true;
    }

    private void apresentaDados() {
        System.out.println("\nPedido de Prestação de Serviços:\n" + m_controller.toString());
    }
}
