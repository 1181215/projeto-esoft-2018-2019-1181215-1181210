/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.gpsd.model;

import java.io.Serializable;

/** Classe representativa da descricao de um serviço
 * @author user
 */
public class DescricaoServicoPedido implements Serializable {

    private String descricao;
    private double duracao;
    private Servico servico;

    /**
     * Construtor da descricao de um serviço com todos os parametros
     * @param serv (uma instancia da classe servico)
     * @param desc (a descricao do servico)
     * @param duracao (a duracao de um servico)
     */
    
    public DescricaoServicoPedido(Servico serv, String desc, double duracao) {
        if ((serv == null) || (desc == null) || (duracao == 0)
                || (desc.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        
        
        this.servico = serv;
        this.descricao = desc;
        this.duracao = duracao;
    }
    /**
     * permite obter o Servico da classe DescricaoServicoPedido
     * @return Servico (instancia da classe servico)
     */
    public Servico getServico() {
        return this.servico;
    }
    /**
     * permite obter a descricao do servico
     * @return descricao (a descrição de um serviço)
     */
    public String getDescricao() {
        return this.descricao;
    }
    /**
     * permite obter a duracao de um serviço
     * @return duracao (a duração de um serviço)
     */
    public double getDuracao() {
        return this.duracao;
    }

    
    /**
     * permite obter o custo de um serviço
     * @return custoDeServico (representa o custo que uma instância da classe DescricaoServicoPedido tem associado
     */
    public double getCusto() {

        double cd = this.servico.getCustoParaDuracao(this.duracao);
        return cd;

    }
}
